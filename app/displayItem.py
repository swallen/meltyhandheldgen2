#!/usr/bin/env python3
from PIL import Image, ImageDraw, ImageFont

#this is a default callback so we don't need to worry about calling nothing if a button without a callback is pressed
def noop():
	pass

class DisplayItem:
    def __init__ (self, posX, posY, width, height, **kwargs):
        self.x = posX
        self.y = posY
        self.h = height
        self.w = width
        self.itemType = kwargs.get('itemType', '')
        self.needsUpdate = True

        #default values
        self.img = Image.new("RGB", (self.w, self.h))
        self.drw = ImageDraw.Draw(self.img)
        self.bg = (0,0,0)
        self.border = 0
        self.text = ""
        self.textSize = 10
        self.textColor = (0,0,0)
        self.callback = noop

        #slide control specific options
        self.knobLength = kwargs.get('knobLength', 30)
        self.knobColor = kwargs.get('knobColor', (230,205,230))
        self.knobValue = kwargs.get('knobStart', 0)

    def setImage(self, image):
        self.img = image

    def setBackground(self, color):
        self.bg = color

    def setBorder(self, thickness):
        self.border = thickness

    def setText(self, text, size, color):
        self.text = text
        self.textSize = size
        self.textColor = color

    def setKnobValue(self, point):
        self.knobValue = max(min((point[1]-self.knobLength/2-self.y)/(self.h-self.knobLength),1),0) 

    def getKnobValue(self):
        return self.knobValue

    def setCallback(self, method):
        self.callback = method

    def isPointInBounds(self, point):
        return (point[0] > self.x and point[0] < (self.x + self.w)) \
            and (point[1] > self.y and point[1] < (self.y + self.h))
    
    def runCallbackIfPoint(self, point):
        if(self.isPointInBounds(point)):

            if(self.itemType == 'slider'):
                self.setKnobValue(point)
                self.needsUpdate = True
            
            if(self.callback != None):
                self.callback()

    def updateImage(self):
        if(self.itemType == 'image'):
            #images are updated manually, externally
            return
        self.drw.rectangle((0,0,self.w,self.h), fill = self.bg)
        if(self.border > 0): #TODO: FIX
            self.drw.rectangle((0,0,self.border,self.h), fill = (0,0,0))
            self.drw.rectangle((0,0,self.w,self.border), fill = (0,0,0))
            self.drw.rectangle((self.w-self.border,0,self.border,self.h), fill = (0,0,0))
            self.drw.rectangle((0,self.h-self.border,self.w,self.border), fill = (0,0,0))
        if(self.itemType == 'slider'):
            knobStartY = self.knobValue*(self.h-self.knobLength)
            self.drw.rectangle((0,knobStartY,self.w,knobStartY+self.knobLength), fill = self.knobColor)
        if(self.text != ""):
            font = ImageFont.truetype("usr/shar/fonts/truetype/liberation/LiberationSans-Regular.ttf", self.textSize)
            w,h = self.drw.textsize(self.text, font=font)
            self.drw.text(((self.w-w)/2, (self.h-h)/2), self.text, fill = self.textColor, font=font)
