#!/usr/bin/python3
import time
import busio
import digitalio
import board
import numpy
from adafruit_rgb_display import hx8357, color565
import adafruit_stmpe610
import smbus
import serial
from PIL import Image
import numpy as np
import math
from multiprocessing import Process, Queue
import traceback
import struct

from displayItem import DisplayItem
from camera import Camera

#return milliseconds since the epoch
def millis():
    return time.time() * 1000

#def haloCallback():
#    global itemList, haloList, displayMode
#    displayMode = 1
#    itemList = haloList
#    disp.fill(0)
#    for item in itemList:
#        item.needsUpdate = True

serialPacketIn = bytearray(256)
serialBytesRead = 0
serialStartFound = 0
dataFile = open("spin_data.csv", "w")

directionTime = 0
direction = 0

def directionCallback():
    global directionTime, direction
    if(millis() - directionTime > 1000):
        directionTime = millis()
        direction = 1 - direction

orientationTime = 0
orientation = 0

def orientationCallback():
    global orientationTime, orientation
    if(millis() - orientationTime > 1000):
        orientationTime = millis()
        orientation = 1 - orientation

#initialize the dead man's switch
if __name__ == '__main__':
    deadman = digitalio.DigitalInOut(board.D23)
    deadman.direction = digitalio.Direction.INPUT
    deadman.pull = digitalio.Pull.UP


    #initialize the touchscreen pins
    cs_pin = digitalio.DigitalInOut(board.CE0)
    st_cs_pin = digitalio.DigitalInOut(board.CE1)
    dc_pin = digitalio.DigitalInOut(board.D25)
    reset_pin = digitalio.DigitalInOut(board.D24)

    BAUDRATE = 24000000

    spi = busio.SPI(clock=board.SCK, MOSI=board.MOSI, MISO=board.MISO)

    i2cBus = smbus.SMBus(1)

    #initialize the touchscreen objects
    st = adafruit_stmpe610.Adafruit_STMPE610_SPI(spi, st_cs_pin)

    #initialize the display objects
    disp = hx8357.HX8357(spi, rotation=0, cs=cs_pin, dc=dc_pin, rst=reset_pin, baudrate=BAUDRATE)

    if disp.rotation % 180 == 90:
        height = disp.width
        width = disp.height
    else:
        width = disp.width
        height = disp.height
    print("Width: " + str(width) + ", height: " + str(height))

    #initialize the radio connection
    radio = serial.Serial('/dev/ttyUSB0', baudrate=57600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)


    cameraDisplay = DisplayItem(80,30,260,260, itemType='image')
    cameraDisplay.setText("Camera feed", 16, (0,0,0))
    cameraDisplay.setBackground((255,230,255))

    
    orientationSelect = DisplayItem(80+260,0,140,80)
    orientationSelect.setBackground((255, 230, 230))
    orientationSelect.setText("Orientation", 16, (0,0,0))
    orientationSelect.setCallback(orientationCallback)

    throttle = DisplayItem(0, 0, 80, height, itemType='slider', knobLength=60, knobColor=(180,180,180), knobStart=1)
    throttle.setBackground((255, 230, 255))
    throttle.setText("Throttle", 16, (0,0,0))

    header = DisplayItem(80,0,260, 30)
    header.setText("MeltyHandheld V2.2", 16, (255,255,255))
    header.setBackground((180, 30, 30))

    directionSelect = DisplayItem(80+260,80, 140, 80)
    directionSelect.setBackground((230, 215, 215))
    directionSelect.setText("Direction", 16, (0,0,0))
    directionSelect.setCallback(directionCallback)


    vBattHeader = DisplayItem(80+260, 160, 140, 20)
    vBattHeader.setBackground((255,230,255))
    vBattHeader.setText("Battery Voltage", 12, (0,0,0))

    vBatt = DisplayItem(80+260, 180, 140, 40)
    vBatt.setBackground((255,230,255))
    vBatt.setText("0.00V", 16, (0,0,0))

    rpmHeader = DisplayItem(80+260, 220, 140, 20)
    rpmHeader.setBackground((255,230,255))
    rpmHeader.setText("RPM", 12, (0,0,0))

    rpm = DisplayItem(80+260, 240, 140, 40)
    rpm.setBackground((255,230,255))
    rpm.setText("0", 16, (0,0,0))

    itemList = [header, cameraDisplay, orientationSelect, throttle, directionSelect, vBattHeader, vBatt, rpmHeader, rpm]

    sendTime = millis()
    imageTime = millis()
    stickX = int(0)
    stickY = int(0)

    #clear the screen
    disp.fill(0)

    queue = Queue()
    camera = Camera(queue)
    camera.start()

    correction = 0

    try:
        while True:
            loopTime = millis()

            if(not queue.empty()):
                cameraData = queue.get()
                correction = cameraData[0]
                cameraDisplay.setImage(Image.fromarray(cameraData[1]))
                cameraDisplay.needsUpdate = True

            #read in from the radio
            while(radio.in_waiting > 0):
                if(serialStartFound):
                    serialPacketIn[serialBytesRead] = ord(radio.read())
                    serialBytesRead += 1

                    if(serialBytesRead == 6):
                        serialStartFound = 0
                        '''
                        beaconTime = int.from_bytes(serialPacketIn[:4], "big")
                        accelMeas = int.from_bytes(serialPacketIn[4:6], "big")

                        if((not deadman.value) and (beaconTime > 0) and (accelMeas > 0)):
                            strVal = str(beaconTime) + "," + str(accelMeas)
                            print(strVal)
                            dataFile.write(strVal + "\n")
                        '''
                        vBattRaw = int.from_bytes(serialPacketIn[:2], "big")
                        usPerDegree = int.from_bytes(serialPacketIn[2:6], "big")

                        vBattVal = vBattRaw/62
                        vBatt.setText('%.2f' % vBattVal, 16, (0,0,0))
                        vBatt.needsUpdate = True

                        if(usPerDegree > 0):
                            speed = 166667/usPerDegree
                        else:
                            speed = 0;
                        rpm.setText(str(int(speed+0.5)), 16, (0,0,0))
                        rpm.needsUpdate = True

                else:
                    if(radio.read() == b'~'):
                        serialStartFound = 1
                        serialBytesRead = 0
            dataFile.flush()


            #check the periodic task for bot control data handling
            if(loopTime - sendTime > 100):
                sendTime = loopTime

                #read in the joystick position
                try:
                    stickXBytes = i2cBus.read_i2c_block_data(0x20, 0x03, 2)
                    stickX = int(((stickXBytes[0] << 8) + stickXBytes[1]) >> 6)
                    stickYBytes = i2cBus.read_i2c_block_data(0x20, 0x05, 2)
                    stickY = int(((stickYBytes[0] << 8) + stickYBytes[1]) >> 6)
                except:
                    pass #sometimes the joystick errors out on read
        
                if st.touched:
                    touchPoint = []

                    # throw out all other touches from this round
                    while not st.buffer_empty:
                        p = st.touches
                        if(len(p) > 0):
                            touchPoint = p
    
                    if(len(touchPoint) > 0):
                        #translate the touch point to screen coordinates
                        #print(touchPoint[0])
                        touchPoint = [(touchPoint[0]['y'] - 235)/7.4458, 320 - (touchPoint[0]['x'] - 261)/9.8969]
                        #print(touchPoint)

                        #now find the item that was touched
                        for item in itemList:
                            item.runCallbackIfPoint(touchPoint)

                #form a packet to send out the radio
                packet = bytearray()
                packet.append(0x7E)#start byte
                packet.append( ((direction) | (orientation << 1)).to_bytes(1, byteorder='big')[0] )#status byte
                packet.append(stickX.to_bytes(2, byteorder='big')[0])
                packet.append(stickX.to_bytes(2, byteorder='big')[1])
                packet.append(stickY.to_bytes(2, byteorder='big')[0])
                packet.append(stickY.to_bytes(2, byteorder='big')[1])
                throt = int(1023*(1-throttle.getKnobValue())).to_bytes(2, byteorder='big')
                packet.append(throt[0])
                packet.append(throt[1])
                packet.append(int(correction).to_bytes(2, byteorder='big')[0])
                packet.append(int(correction).to_bytes(2, byteorder='big')[1])
                if(deadman.value):
                    saveEnable = False
                    packet.append(0x00)#disabled
                else:
                    saveEnable = True
                    packet.append(0xAA)#enabled
                #print(packet)
                radio.write(packet)
                radio.flushOutput()

            #if something on the screen changed, draw a new screen
            for item in itemList:
                if(item.needsUpdate):
                    item.needsUpdate = False
                    item.updateImage()
                    disp.image(item.img, y=item.y, x=item.x)
            time.sleep(0.0001)
    except Exception as e:
        traceback.print_exc()
        print(e)
        camera.shutdown()


