#!/usr/bin/python3
import time
import picamera
import picamera.array
import cv2
from PIL import Image
import numpy as np
import math
from multiprocessing import Process, Manager, Event

#return milliseconds since the epoch
def millis():
    return time.time() * 1000

class ContourAnalyser(Process):
    def __init__(self, inQ, outQ):
        super(self.__class__, self).__init__()
        self.inQ = inQ
        self.outQ = outQ
        self.exit = Event()

    def getContours(self, img):
        thresh = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 101, 3)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
        blob = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
        #kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (9,9))
        #blob = cv2.morphologyEx(blob, cv2.MORPH_CLOSE, kernel)
        #print(f"Morphology2: {millis()-startTime}")

        contours, hierarchy = cv2.findContours(blob, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        filtered = []
        for cnt in contours:
            approxCurve = cv2.approxPolyDP(cnt, 0.04*cv2.arcLength(cnt, True), True)
            if(cv2.contourArea(cnt) < 1000):
                continue
            if(cv2.isContourConvex(approxCurve)):
                continue
            filtered.append(cnt)

        if(not filtered):
            return -1
        return filtered

    def run(self):
        while(not self.exit.is_set()):
            if(not self.inQ.empty()):
                self.outQ.put(self.getContours(self.inQ.get()))
            time.sleep(0.0001)

    def shutdown(self):
        self.exit.set()



class MyAnalysisClass(picamera.array.PiRGBAnalysis):
    def __init__(self, camera):
        super(self.__class__, self).__init__(camera)
        self.correction = 0;
        self.processedImage = []
        self.hasImage = False
        
        self.manager = Manager()

        self.toQ0 = self.manager.Queue()
        self.fromQ0 = self.manager.Queue()
        self.contourAnalyser0 = ContourAnalyser(self.toQ0, self.fromQ0)

        self.toQ1 = self.manager.Queue()
        self.fromQ1 = self.manager.Queue()
        self.contourAnalyser1 = ContourAnalyser(self.toQ1, self.fromQ1)

        self.contourAnalyser0.start()
        self.contourAnalyser1.start()

        self.frameIndex = 0


    def imageReady(self):
        return self.hasImage

    def getData(self):
        self.hasImage = False
        return [self.correction, self.processedImage]

    def analyse(self, array):
        #array = cv2.imread('lastCapture.png')
        #startTime = millis()
        #cv2.imwrite("capture_" + str(self.frameIndex) + ".png", array)
        #self.frameIndex += 1

        hsv = cv2.cvtColor(array, cv2.COLOR_RGB2HSV)

        red_mask0 = cv2.inRange(hsv, (0,25,25), (10,255,255))
        red_mask1 = cv2.inRange(hsv, (170,25,25), (180,255,255))
        red_mask = red_mask0 + red_mask1
        red_color = cv2.bitwise_and(array, array, mask=red_mask)
        redImg = cv2.cvtColor(red_color, cv2.COLOR_RGB2GRAY)

        green_mask = cv2.inRange(hsv, (36,25,25), (70,255,255))
        green_color = cv2.bitwise_and(array, array, mask=green_mask)
        greenImg = cv2.cvtColor(green_color, cv2.COLOR_RGB2GRAY)

        #print(f"Masking: {millis()-startTime}")

        self.toQ0.put(redImg)
        self.toQ1.put(greenImg)
        while(self.fromQ0.empty()):
            pass
        redContours = self.fromQ0.get()
        while(self.fromQ1.empty()):
            pass
        greenContours = self.fromQ1.get()
        #print(f"Contour Processing: {millis()-startTime}")

        if(isinstance(redContours, list) and isinstance(greenContours, list)):
            closestDist = 1000000
            closestX = 0
            closestY = 0
            finalGreen = []
            finalRed = []
            for red in redContours:
                for green in greenContours:
                    rx,ry,rw,rh = cv2.boundingRect(red)
                    gx,gy,gw,gh = cv2.boundingRect(green)
                    dispX = rx + rw/2 - gx - gw/2
                    dispY = ry + rh/2 - gy - gh/2
                    disp = math.sqrt(math.pow(dispX,2)+math.pow(dispY,2))
                    if(disp < closestDist):
                        closestDist = disp
                        closestX = dispX
                        closestY = dispY
                        finalGreen = green
                        finalRed = red
            #print(f"Contour Matching: {millis()-startTime}")

            angle = math.atan2(closestY, closestX)

            rx,ry,rw,rh = cv2.boundingRect(finalRed)
            gx,gy,gw,gh = cv2.boundingRect(finalGreen)
            cR = (int(rx+rw/2),int(ry+rh/2))
            cG = (int(gx+gw/2),int(gy+gh/2))
            cC = (int(cR[0]/2+cG[0]/2), int(cR[1]/2+cG[1]/2))
            cO = (int(cC[0] + math.cos(angle + math.radians(90))*200), int(cC[1] + math.sin(angle + math.radians(90))*200))

            processedImg = cv2.drawContours(array, [finalGreen, finalRed], -1, (255,0,0), 3)
            processedImg = cv2.circle(processedImg, cR, 6, (255,0,0), -1)
            processedImg = cv2.circle(processedImg, cG, 6, (255,0,0), -1)
            processedImg = cv2.line(processedImg, cR, cG, (255,0,0), thickness=3)
            processedImg = cv2.line(processedImg, cC, cO, (255,0,0), thickness=3)
            #print(f"Contour Drawing: {millis()-startTime}")

            self.correction = math.degrees(angle)
            self.processedImage = cv2.resize(processedImg, (260,260))
        else:
            self.correction = 0
            self.processedImage = cv2.resize(array, (260,260))

        while(self.correction < 0):
            self.correction = self.correction + 360
        self.hasImage = True

    def shutdown(self):
        self.contourAnalyser0.shutdown() 
        self.contourAnalyser1.shutdown()
        self.contourAnalyser0.join()
        self.contourAnalyser1.join()

class Camera(Process):
    def __init__(self, queue):
        super(self.__class__, self).__init__()
        self.queue = queue
        self.exit = Event()

    def run(self):
        #1640x1232 (mode 4) (1/10 <= fps <= 40)
        cam = picamera.PiCamera()
        cam.resolution = (1640, 1232)
        cam.framerate = 20
        cam.shutter_speed = 200000
        cam.rotation = 90
        camera_output = MyAnalysisClass(cam)

        cam.start_recording(camera_output, format="rgb", splitter_port=1)
        while(not self.exit.is_set()):
            if(camera_output.imageReady()):
                self.queue.put(camera_output.getData())
            time.sleep(0.0001)
        cam.stop_recording(splitter_port=1)
        time.sleep(1)
        cam.close()
        camera_output.shutdown()

    def shutdown(self):
        self.exit.set()

