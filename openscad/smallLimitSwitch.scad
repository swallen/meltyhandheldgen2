fid = 100;

bounds = [20, 10.5, 6.5];

holeYDisp = 6.5;
hole1XDisp = 5;
hole2XDisp = 14.5;

holeR = 2.5/2;

tabBounds = [18.5, 0.3, 4.2];
tabDisp = [-2.5, -1, 1.3];

rollerR = 4/2;
rollerDisp = 5.6-rollerR-tabBounds[1];

smallLimitSwitch();

module smallLimitSwitch() {
    difference() {
        cube(bounds);
        
        translate([hole1XDisp, holeYDisp, -1])
            cylinder(bounds[2]+2, r=holeR, $fn=fid);
        translate([hole2XDisp, holeYDisp, -1])
            cylinder(bounds[2]+2, r=holeR, $fn=fid);
    }
    
    translate(tabDisp) cube(tabBounds);
    
    translate(tabDisp + [rollerR,-rollerDisp,0])
        cylinder(tabBounds[2], r=rollerR, $fn=fid);
}