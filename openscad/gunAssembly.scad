use <battery.scad>
use <touchscreen_3_5.scad>
use <thumbstick.scad>
use <xbeeExplorer.scad>
use <rpi4.scad>
use <rpiCam.scad>
use <smallLimitSwitch.scad>
use <gunGrip.scad>

gunGrip();

rpiPos() {
    rpi4();
    translate([85,56,4+13]) rotate([0,0,180]) touchscreen_3_5();
}

thumbstickPos() translate([26,0,0]) rotate([0,0,90]) thumbstick();

translate([64.25/2, 20+95/2, 23.25]) rotate([0,0,-90]) rotate([180,0,0]) battery();

//translate([55, 25,-3]) rotate([0,180,180]) rpiCam();


radioPos() translate([35.5,0,0]) rotate([90,0,180]) xbeeExplorer();

limitSwitchPos() translate([0,20,1]) rotate([180,-90,0]) smallLimitSwitch();