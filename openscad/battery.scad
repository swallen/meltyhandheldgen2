fid = 100;

depth = 23.25;
height = 64.25;
width = 95;
cornerRad = 7.5;

dispHeight = 16.5;
dispWidth = 13;
dispCurveDist = 3;
dispTopDist = 9.5;
dispLeftDist = 9.75;

inOutLeftDist = 13;
inOutZDisp = 10;

usbAWidth = 13;
usbAHeight = 5.5;
usbA1YDisp = 11;
usbA2YDisp = 41;
usbAZDisp = 8.5;

usbMiniWidth = 8.5;
usbMiniHeight = 3;
usbMiniYDisp = 28.5;
usbMiniZDisp = 10;

battery();

module battery() color([0.3, 0.3, 0.3]) difference() {
    roundedCube(cornerRad, [width, height, depth]);
    
    //make an inset to show the battery % display
    translate([dispLeftDist+dispWidth/2, height-dispTopDist, depth-1]) hull() {
        resize([dispWidth, dispCurveDist, 2])
            cylinder(1, r=1, $fn=fid);
        translate([0,-dispHeight+dispCurveDist*2,0])
            resize([dispWidth, dispCurveDist, 2])
            cylinder(1, r=1, $fn=fid);
    }
    
    //USB-C in/out power port
    translate([inOutLeftDist, -1, inOutZDisp])
        rotate([-90,0,0]) usbCPlugCutout();
    
    //USB-A power out ports
    translate([0, usbA1YDisp, usbAZDisp])
        cube([5, usbAWidth, usbAHeight]);
    translate([0, usbA2YDisp, usbAZDisp])
        cube([5, usbAWidth, usbAHeight]);
    
    //USB-mini power in port
    translate([0, usbMiniYDisp, usbMiniZDisp])
        cube([5, usbMiniWidth, usbMiniHeight]);
}
    
module roundedCube(r, vec) {
    hull() {
        translate([r,r,r]) sphere(r, $fn=fid);
        translate([vec[0]-r,r,r]) sphere(r, $fn=fid);
        translate([r,vec[1]-r,r]) sphere(r, $fn=fid);
        translate([r,r,vec[2]-r]) sphere(r, $fn=fid);
        translate([vec[0]-r,vec[1]-r,r]) sphere(r, $fn=fid);
        translate([vec[0]-r,r,vec[2]-r]) sphere(r, $fn=fid);
        translate([r,vec[1]-r,vec[2]-r]) sphere(r, $fn=fid);
        translate([vec[0]-r,vec[1]-r,vec[2]-r]) sphere(r, $fn=fid);
    }
}

module usbCPlugCutout() {
    usbCIndentR = 7/2;
    usbCIndentLength = 12.5;
    usbCPlugR = 3/2;
    
    translate([usbCIndentR,0,0]) {
        hull() {
            cylinder(2, r=usbCIndentR, $fn=fid);
            
            translate([usbCIndentLength-usbCIndentR*2,0,0])
                cylinder(2, r=usbCIndentR, $fn=fid);
        }
        
        hull() {
            cylinder(6, r=usbCPlugR, $fn=fid);
            
            translate([usbCIndentLength-usbCIndentR*2,0,0])
                cylinder(6, r=usbCPlugR, $fn=fid);
        }
    }
}