use <battery.scad>
use <touchscreen_3_5.scad>
use <thumbstick.scad>
use <xbeeExplorer.scad>
use <rpi4.scad>
use <rpiCam.scad>
use <shell.scad>
use <smallLimitSwitch.scad>

//bottomShell();

translate([(64.25-56)/2,(95-85)/2,24]) {
    rpi4();
    translate([85,56,4+13]) rotate([0,0,180]) touchscreen_3_5();
    
    translate([0,0,19]) {
        topShell();
        
        %translate(bottomCoverDisp()) bottomCover();
    }
}

translate([95,1,23.25]) rotate([0,0,180]) rotate([180,0,0]) battery();

translate([55, 25,-3]) rotate([0,180,180]) rpiCam();

translate([0, -26-5, 32]) thumbstick();

translate([55, -30, 32]) rotate([0,0,0]) xbeeExplorer();

translate([-20, 100, 10]) smallLimitSwitch();