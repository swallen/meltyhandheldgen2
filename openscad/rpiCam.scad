fid = 100;

length = 24;
width = 25;
depth = 1;

mountHoleR = 2.5/2;
mountHoleDisp = 2;
mountHoleXDisp1 = 9.5;
mountHoleLocs = [[mountHoleXDisp1, mountHoleDisp],
                [mountHoleXDisp1, width-mountHoleDisp],
                [length-mountHoleDisp, mountHoleDisp],
                [length-mountHoleDisp, width-mountHoleDisp]];
                
camXDisp = 5;
camYDisp = 8.5;
camLength = 8.5;
camWidth = 8.5;
camDepth = 5;

cableLength = 7.5;
cableWidth = 9;
cableDepth = 1.5;
cableXDisp = camXDisp+camLength;
cableYDisp = 9.5;

connLength = 5.5;
connWidth = 21.5;
connDepth = 2.5;
connYDisp = 2;

rpiCam();

module rpiCam() {
    difference() {
        //pcb
        color([0.5,0.8,0.4]) cube([length, width, depth]);
        
        //mount holes
        for(loc = mountHoleLocs) {
            translate([loc[0], loc[1], -1]) cylinder(2+depth, r=mountHoleR, $fn=fid);
        }
    }
    
    //camera body
    translate([camXDisp, camYDisp, depth]) cube([camLength, camWidth, camDepth]);
    
    //camera ribbon cable to pcb
    translate([cableXDisp, cableYDisp, depth]) cube([cableLength, cableWidth, cableDepth]);
    
    //external ribbon cable connector
    translate([0,connYDisp,-connDepth]) cube([connLength, connWidth, connDepth]);
}

function rpiCamMountHoles() = mountHoleLocs;

