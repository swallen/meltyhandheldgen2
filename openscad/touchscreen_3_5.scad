fid = 100;

length = 85;
width = 56;
depth = 5.5;

tabLength = 6.5;
tabWidth = 6.5;
tabDepth = 1.6;

mountHoleR = 2.5/2;

headerXDisp = 27;
headerYDisp = 1.8;
headerLength = 51;
headerWidth = 5;
headerDepth = 13;

header2Bound = [5, 32.7, 9.3];
header2Disp = [72.5, 12.5, -header2Bound[2]];

touchscreen_3_5();

module touchscreen_3_5 () {
    color([0.1,0.2,0.6]) {
        cube([length, width, depth]);
    
        mountTab([length,0,0]);
        mountTab([length,width-tabWidth,0]);
        mountTab([-tabLength,0,0]);
        mountTab([-tabLength,width-tabWidth,0]);
    }
    
    color([0.3, 0.3, 0.3]) translate([headerXDisp, headerYDisp, -headerDepth]) 
        cube([headerLength, headerWidth, headerDepth]);
    
    color([0.3, 0.3, 0.3]) translate(header2Disp) cube(header2Bound);
}

module mountTab(vec) {
    
    translate(vec) difference() {
        cube([tabLength, tabWidth, tabDepth]);
        
        translate([tabLength/2, tabWidth/2, -1]) cylinder(tabDepth+2, r=mountHoleR, $fn=fid);
    }
}

module touchscreen_3_5_cutout(height) {
    cube([length, width, height]);
    
    holeLocs = [[-tabLength/2, tabWidth/2],
                [-tabLength/2, width-tabWidth/2],
                [length+tabLength/2, tabWidth/2],
                [length+tabLength/2, width-tabWidth/2]];
    
    for(loc = holeLocs) {
        translate([loc[0], loc[1], 0]) 
            cylinder(height, r=mountHoleR, $fn=fid);
    }
}