fid = 100;

use <thumbstick.scad>
use <rpi4.scad>
use <xbeeExplorer.scad>

gripR = 20;
gripDisp = 30;
gripH = 91;
gripLen = sqrt(pow(gripDisp,2)+pow(gripH,2));

wall = 4;
batteryDim = [64.25, 95, 23.25];

limitSwitchDim = [7, 22, 22];

gunGrip();

module gunGrip() {
    
    difference() {
        //the grip stem
        translate([0,0,batteryDim[2]+wall]) rotate([-atan2(gripDisp, gripH),0,0]) {
            translate([0,0,-10]) cylinder(gripLen+10, r=gripR, $fn=fid);
        }
        
        //cut off under the battery plate
        translate([-50, -50, batteryDim[2]-50]) cube([100, 100, 50]);
        
        //cutout for the limit switch
        limitSwitchPos() {
            //the body of the switch
            cube(limitSwitchDim);
            
            //the mount holes
            translate([-50,12,7]) rotate([0,90,0]) cylinder(50, r=3/2, $fn=fid);
            translate([-50,12,17]) rotate([0,90,0]) cylinder(50, r=3/2, $fn=fid);
            
            //cutouts for the mount bolt heads
            translate([-gripR,12,7]) rotate([0,90,0]) cylinder(gripR-wall, r=8/2, $fn=fid);
            translate([-gripR,12,17]) rotate([0,90,0]) cylinder(gripR-wall, r=8/2, $fn=fid);
        }
        
        //the grip grooves
        translate([0,0,batteryDim[2]]) for(disp=[18.5,43.5,68.5]) {
            for(ang=[0:10:180]) {
                rotate([-atan2(gripDisp, gripH),0,0]) 
                    rotate([0,0,ang]) translate([gripR+20,0,disp]) 
                    sphere(r=25, $fn=fid);
            }
        }
    }
    
    //the plate that rests on the battery
    translate([-batteryDim[0]/2, 20-batteryDim[1]/2, batteryDim[2]])
        cube([batteryDim[0], batteryDim[1], wall]);
    
    translate([0,20,batteryDim[2]]) difference() {
        hull() {
            translate([-4-batteryDim[0]/2, 0, 0]) cylinder(wall, r=8/2, $fn=fid);
            translate([4+batteryDim[0]/2, 0, 0]) cylinder(wall, r=8/2, $fn=fid);
            translate([0,-4-batteryDim[1]/2, 0]) cylinder(wall, r=8/2, $fn=fid);
            translate([0,4+batteryDim[1]/2, 0]) cylinder(wall, r=8/2, $fn=fid);
        }
        translate([-4-batteryDim[0]/2,0,-1]) cylinder(wall+2, r=3/2, $fn=fid);
        translate([4+batteryDim[0]/2,0,-1]) cylinder(wall+2, r=3/2, $fn=fid);
        translate([0,4+batteryDim[1]/2,-1]) cylinder(wall+2, r=3/2, $fn=fid);
        translate([0,-4-batteryDim[1]/2,-1]) cylinder(wall+2, r=3/2, $fn=fid);
    }

    //the thumbstick mount
    thumbstickPos() difference() {
        translate([0,0,-10]) cube([26,51.5,10]);
        rotate([180,0,90]) thumbstickMountHoles();
    }
    
    //the raspberry pi mount
    difference() {
        union() {
            rpiPos() translate([0,0,-3]) cube([85, 56, wall]);
            
            //support for the pi mount
            hull() {
                rpiPos() translate([0,0,-3]) cube([85, 1, 3]);
                thumbstickPos() translate([0,25,-3]) cube([26, 25, 3]);
            }
        }
        
        //cutout the mount holes
        rpiPos() rpi4MountHoles();
        
        //cutout the thumbstick profile so we can mount it
        thumbstickPos() translate([-2,0,0]) cube([26+4,51.5,50]);
    }
    
    //the radio mount
    difference() {
        radioPos() translate([0,-5,0]) cube([35.5, 5, 10]);
        
        radioPos() rotate([90,0,0]) xbeeExplorerMountHoles();
    }
    
    //support strut
    hull() {
        thumbstickPos() translate([26/2, 51.5, -5]) sphere(r=8/2, $fn=fid);
        rpiPos() translate([85/2, 56/2+20, -4]) sphere(r=7/2, $fn=fid);
    }
}

module limitSwitchPos() {
    translate([0,0,wall+23.25]) rotate([-atan2(gripDisp, gripH),0,0])
    translate([-limitSwitchDim[0]/2,gripR-limitSwitchDim[1],gripLen-limitSwitchDim[2]])
        children();
}

module thumbstickPos() {
    gripStemEnd() rotate([45,0,0]) translate([0,2,-5]) translate([-26/2,0,3]) children();
}

module rpiPos() {
    gripStemEnd() rotate([60,0,0]) translate([-85/2,40,5]) children();
}

module radioPos() {
    gripStemEnd() translate([-35.5/2,46,29]) rotate([-20,0,0]) children();
}

module gripStemEnd() {
    translate([0,gripDisp,batteryDim[2]+wall+gripH]) children();
}