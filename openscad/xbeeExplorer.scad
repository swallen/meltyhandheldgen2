fid = 100;

pcbWidth = 28.5;
pcbLength = 35.5;
pcbThick = 1.68;

holeXDisp1 = 2.5;
holeXDisp2 = 32.75;
holeYDisp1 = 4;
holeYDisp2 = 25;

holeR = 3.5/2;

usbLength = 8.5;
usbWidth = 7.7;
usbDepth = 3.9;

holes = [ [holeXDisp1, holeYDisp1],
          [holeXDisp1, holeYDisp2],
          [holeXDisp2, holeYDisp1],
          [holeXDisp2, holeYDisp2] ];
          
headerXDisp = 7;
headerYDisp = 2;
headerLength = 20.5;
headerWidth = 2.5;
headerDepth = 7.5;

xbeeDepth = 4;
xbeeTabLength = 7;
xbeeTabWidth = 9.5;

xbeeExplorer();

module xbeeExplorer() {
    //pcb+mount holes
    difference() {
        color([1,0,0]) cube([pcbLength, pcbWidth, pcbThick]);
        
        xbeeExplorerMountHoles();
    }
    
    //usb port
    translate([0, pcbWidth/2-usbWidth/2, pcbThick])
        color([0.7, 0.7, 0.7]) cube([usbLength, usbWidth, usbDepth]);
    
    translate([headerXDisp, headerYDisp, pcbThick])
        color([0.3, 0.3, 0.3]) cube([headerLength, headerWidth, headerDepth]);
    
    translate([headerXDisp, pcbWidth-headerYDisp-headerWidth, pcbThick])
        color([0.3, 0.3, 0.3]) cube([headerLength, headerWidth, headerDepth]);
    
    translate([headerXDisp, headerYDisp, pcbThick+headerDepth])
        color([0.3, 0.3, 0.3]) cube([headerLength, pcbWidth-headerYDisp*2, xbeeDepth]);
    //TODO: TRAPEZOIDAL TAG
}

function xbeeExplorerMountHolePos() = holes;

module xbeeExplorerMountHoles() {
    for(pos = holes) {
        translate([pos[0],pos[1],-50]) cylinder(100, r=holeR, $fn=fid);
    }
}