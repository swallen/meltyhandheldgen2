depth = 17;
width = 190;
height = 115;
topMargin = 15;
sideMargin = 18;

touchscreen();

module touchscreen() color([0.3, 0.3, 0.3]) difference() {
    cube([width, height, depth]);
    
    translate([sideMargin, topMargin, depth-1]) 
        cube([width-sideMargin*2, height-topMargin*2, 2 ]);
    
    
}