fid = 100;

use <touchscreen_3_5.scad>;
use <thumbstick.scad>;
use <xbeeExplorer.scad>;

wall = 3;
wallHeight = 49;

facePlateBounds = [100+wall*2, 102, wall+wallHeight];
touchScreenBounds = [85, 56, 3];
facePlateDisp = [-(facePlateBounds[0]-touchScreenBounds[0])/2, 
                5+wall-(facePlateBounds[1]-touchScreenBounds[1]), 0];
                
cornerR = 5;


thumbStickOffset = [touchScreenBounds[0]/2-35, -23, 0];
xbeeExplorerOffset = [50, -35, 32];

bumpDist = 15;

bumpDispFromWall = (pow((wallHeight+wall*2)/2,2) - pow(bumpDist,2))/(2*bumpDist);
echo(bumpR);
bumpR = bumpDist+bumpDispFromWall;

function bottomCoverDisp() = [facePlateDisp[0], facePlateDisp[1], -wallHeight- wall];

wedgeHeight = 10;
wedgeSide = 6;
coverBoltDisp = facePlateDisp + [0,0,-wallHeight];
coverBoltPoints =  [coverBoltDisp + [wedgeSide/2,wedgeSide/2,0]+[wall, 30, 0],
                    coverBoltDisp + [-wedgeSide/2,wedgeSide/2,0]+[facePlateBounds[0]-wall, 30, 0],
                    coverBoltDisp + [wedgeSide/2,-wedgeSide/2,0]+[wall, facePlateBounds[1]-wall, 0],
                    coverBoltDisp + [-wedgeSide/2,-wedgeSide/2,0]+[facePlateBounds[0]-wall, facePlateBounds[1]-wall,0]];

//topShell();

translate(bottomCoverDisp()) color([0.2,0.2,0.2]) bottomCover();

module topShell() {
    difference() {
        //main body
        translate(facePlateDisp) {
            //face plate
            cube([facePlateBounds[0], facePlateBounds[1], wall]);
            
            //top and bottom walls
            translate([0,0,-wallHeight]) cube([wall, facePlateBounds[1], wallHeight]);
            translate([facePlateBounds[0]-wall,0,-wallHeight]) 
                cube([wall, facePlateBounds[1], wallHeight]);
        }
        
        //cut out the hole for the touchscreen
        translate([0,0,-0.5]) touchscreen_3_5_cutout(wall+1);
        
        //thimbstick cutout
        translate(thumbStickOffset + [0,0,-1]) 
            cylinder(wall+2, r=15, $fn=fid);
    }
    
    
    //add bolt points for the touchscreen
    translate(coverBoltPoints[0]) coverBoltPoint();
    translate(coverBoltPoints[1]) rotate([0,0,180]) coverBoltPoint();
    translate(coverBoltPoints[2]) coverBoltPoint();
    translate(coverBoltPoints[3]) rotate([0,0,180]) coverBoltPoint();
    
    //thumbstick mount pegs
    translate(thumbStickOffset + [-12.5,-13,-10]) {
        holes = thumbstickMountHoles();
        
        translate(holes[2]) mountPeg(10);
        translate(holes[3]) mountPeg(10);
        translate(holes[4]) mountPeg(10);
        translate(holes[5]) mountPeg(10);
    }
    
    //xBee explorer mount
    translate([xbeeExplorerOffset[0], xbeeExplorerOffset[1], -10]) {
        for(loc = xbeeExplorerMountHoles()) {
            translate([loc[0], loc[1], 0]) mountPeg(10);
        }
    }
    
    //left hand grip has extra holes cut out
    translate([facePlateDisp[0], facePlateDisp[1], 0]) difference() {
        curvedGrip();
        
        translate([0, bumpR-bumpDist, -wallHeight/2]) {
            //hole for limit switch
            switchBounds = [20, 10.5, 6.5];
            rotate([150,0,0]) 
                translate([60,0,-switchBounds[2]/2]) 
                cube([switchBounds[0], bumpR, switchBounds[2]]);
            
            //hole for power switch
            powerBounds = [20, 10];
            rotate([210,0,0])
                translate([40,0,-powerBounds[1]/2])
                cube([powerBounds[0], bumpR, powerBounds[1]]);
        }
    }
    
    //right hand grip has no features
    translate([facePlateDisp[0], facePlateDisp[1]+facePlateBounds[1], 0]) 
        mirror([0,1,0]) curvedGrip();
}

module mountPeg(height) {
    difference() {
        cylinder(height, r=6/2, $fn=fid);
        
        cylinder(height, r=3/2, $fn=fid);
    }
}

module curvedGrip() intersection() {
    translate([0,-bumpDist, -wallHeight-wall])
        cube([facePlateBounds[0], bumpDist, wallHeight+2*wall]);
    translate([0,bumpR-bumpDist,-wallHeight/2]) rotate([0,90,0]) difference() {
        cylinder(facePlateBounds[0], r=bumpR, $fn=fid);
        translate([0,0,wall]) cylinder(facePlateBounds[0]-wall*2, r=(bumpR-wall), $fn=fid);
    }
}

module coverBoltPoint() {
    difference() {
        translate([-wedgeSide/2, -wedgeSide/2, 0]) intersection() {
            cube([wedgeSide, wedgeSide, wedgeHeight]);
            translate([0,0,wedgeHeight]) rotate([0,180-atan2(wedgeSide, wedgeHeight),0])
                cube([10, wedgeSide, sqrt(pow(wedgeHeight, 2) + pow(wedgeSide, 2))]);
        }
        
        translate([0, 0, -1]) cylinder(wedgeHeight + 2, r=3/2, $fn=fid);
    }
}

function bottomCoverDisp() = [facePlateDisp[0], facePlateDisp[1], -wallHeight- wall];

module bottomCover() {
    difference() {
        //Grips and plate
        union() {
            cube([facePlateBounds[0], facePlateBounds[1], wall]);
    
            intersection() {
                translate([0, bumpDist+wall, wallHeight/2+wall]) rotate([0,90,0]) 
                    cylinder(facePlateBounds[0], r=bumpR, $fn=fid);
        
                translate([0,0,-50]) cube([200, 200, 50]);
            }
    
            intersection() {
                translate([0, facePlateBounds[1]-bumpDist-wall, wallHeight/2+wall])
                    rotate([0,90,0]) cylinder(facePlateBounds[0], r=bumpR, $fn=fid);
        
                translate([0,0,-50]) cube([200, 200, 50]);
            }
        }
        
        //bolt cutouts
        for(loc = coverBoltPoints) translate(loc - bottomCoverDisp() - [0,0,50]) {
            cylinder(100, r=3/2, $fn=fid);
            cylinder(50-wall, r=8/2, $fn=fid);
        }
        
        //battery read cutout
        translate([78,76,-50]) cube([14,15,100]);
        
        translate([0,36,-50]) cube([wall+2, 20, 100]);
    }
}