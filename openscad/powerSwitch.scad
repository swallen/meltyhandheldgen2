fid = 100;

//model of the Canakit usb-c inline power switch

bounds = [38, 18, 10.5];

buttonDisp = [bounds[0]/2, bounds[1]/2, bounds[2]];
buttonHeight = 1.5;
buttonR = 4.3/2;

ledDisp = [10.5, bounds[1]/2, bounds[2]-0.5];
ledR = 2.5/2;

cableR = 5.7/2;

usbCBounds = [3, 9, 3];
usbCDisp = [bounds[0]-usbCBounds[0], bounds[1]/2-usbCBounds[1]/2, 4];

powerSwitch();

module powerSwitch() {
    difference() {
        color([0.2, 0.2, 0.2]) cube(bounds);
        
        translate(ledDisp) cylinder(1, r=ledR, $fn=fid);
        
        translate(usbCDisp) cube(usbCBounds+[1,0,0]);
    }
    
    translate(buttonDisp) cylinder(buttonHeight, r=buttonR, $fn=fid);
    
    color([0.2, 0.2, 0.2]) translate([0,bounds[1]/2, bounds[2]/2]) 
        rotate([0,-90,0]) cylinder(6.5, r=cableR, $fn=fid);
}