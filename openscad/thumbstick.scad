fid = 100;

pcbLength = 51.5;
pcbWidth = 26;
pcbThick = 1.6;

holeInset = 2.75;
midHoleDisp = 28.25;
holeR = 3.5/2;

holePos = [ [holeInset, holeInset],
            [holeInset, pcbWidth-holeInset],
            [midHoleDisp, holeInset],
            [midHoleDisp, pcbWidth-holeInset],
            [pcbLength-holeInset, holeInset],
            [pcbLength-holeInset, pcbWidth-holeInset] ];

stickR = 10.75/2;
stickHeight = 5.25;
padR = 20/2;
padHeight = 4.5;
ballHeight = 11;
ballBotR = 27/2;
ballTopR = stickR;
ballZDisp = 8.5;
ballXDist = -1;
    
sphereZDist = (pow(ballBotR, 2)-pow(ballTopR, 2)-pow(ballZDisp,2))/(2*ballZDisp);
sphereR = sqrt(pow(ballBotR, 2) + pow(sphereZDist, 2));

thumbstick();

module thumbstick() {
    //the pcb
    difference() {
        color([1,0,0]) cube([pcbLength, pcbWidth, pcbThick]);
        
        for(pos = holePos) {
            translate([pos[0],pos[1],-1]) cylinder(3, r=holeR, $fn=fid);
        }
    }
    echo(ballXDist+ballBotR);

    //the ball, stick and pad
    color([0.3, 0.3, 0.3]) translate([ballXDist+ballBotR,pcbWidth/2,pcbThick]) {
        translate([0,0, ballZDisp+ballHeight])
            cylinder(stickHeight, r=stickR, $fn=fid);
        
        translate([0,0, ballZDisp+ballHeight+stickHeight])
            cylinder(padHeight, r=padR, $fn=fid);
        
        translate([0,0, ballHeight-sphereZDist]) difference() {
            sphere(sphereR, $fn=fid);
            
            translate([0,0,-sphereR/2])
            cube([sphereR*2, sphereR*2, sphereR], center=true);
        }
    }
}

function thumbstickMountHolePos() = holePos;

module thumbstickMountHoles() {
        for(pos = holePos) {
            translate([pos[0],pos[1],-50]) cylinder(100, r=holeR, $fn=fid);
        }
}