fid = 100;

pcbDepth = 1.45;
pcbLength = 85;
pcbWidth = 56;

mountHoleR = 2.6/2;
mountHoleSideDisp = 3.5;
mountHoleXDisp = 61.5;

mountHoleLocs =[[mountHoleSideDisp, mountHoleSideDisp],
                [mountHoleSideDisp, pcbWidth-mountHoleSideDisp],
                [mountHoleXDisp, mountHoleSideDisp],
                [mountHoleXDisp, pcbWidth-mountHoleSideDisp]];

ethernetWidth = 16;
ethernetLength = 21;
ethernetDepth = 13.8;
ethernetXDisp = 66.7;
ethernetYDisp = 38;

usbWidth = 13;
usbLength = 18;
usbDepth = 15;
usbXDisp = 70;
usbYDisp1 = 2;
usbYDisp2 = 20;

usbCBounds = [9, 7.5, 3];
usbCDisp = [6.5, -1.4, pcbDepth];

cameraPortBounds = [2.5, 21, 5.5];
cameraPortDisp = [45.5, 0.2, pcbDepth];

rpi4();

module rpi4 () {
    
    //PCB
    difference() {
        color([0.5,0.8,0.4]) roundedSquare(2.5, [pcbLength, pcbWidth, pcbDepth]);
        
        rpi4MountHoles();
    }
    
    //ethernet
    translate([ethernetXDisp, ethernetYDisp, pcbDepth])
        color([0.7, 0.7, 0.7]) cube([ethernetLength, ethernetWidth, ethernetDepth]);
    
    //usb A
    translate([usbXDisp, 0, pcbDepth]) color([0.7, 0.7, 0.7]) {
        translate([0,usbYDisp1,0]) cube([usbLength, usbWidth, usbDepth]);
        translate([0,usbYDisp2,0]) cube([usbLength, usbWidth, usbDepth]);
    }
    
    //usb C
    translate(usbCDisp) color([0.7, 0.7, 0.7]) cube(usbCBounds);
    
    //camera ribbon cable connection
    translate(cameraPortDisp) cube(cameraPortBounds);
}

module roundedSquare(r, vec) {
    hull() {
        translate([r,r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([vec[0]-r,r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([r,vec[1]-r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([vec[0]-r,vec[1]-r,0]) cylinder(vec[2], r, r, $fn=fid);
    }
}

function rpi4MountHolePos() = mountHoleLocs;

module rpi4MountHoles() {
        for(loc = mountHoleLocs) {
            translate([loc[0], loc[1], -50]) cylinder(100, r=mountHoleR, $fn=fid);
        }
}