fid = 100;

switchDepth = 9.5;
switchLength = 28;
switchWidth = 16;
switchCornerR = 2.8;

mountHoleR = 3/2;
mountHoleDisp = 2.8;

mountHoleLocs = [[mountHoleDisp, switchWidth-mountHoleDisp],
                [switchLength-mountHoleDisp, mountHoleDisp]];
                
tabWidth = 0.5;

tab12Length = 9.5;
tab3Length = 8.5;
tab3XDisp = switchLength-14.5;

tab1YDisp = 3.75;
tab2YDisp = switchWidth-5.35-tabWidth;
tab3YDisp = -3;

tabZDisp = 1.35;
tabDepth = 6.35;

leverLength = 51.3;
leverXDisp = 11;

leverWidth = 0.62;
leverYDisp = switchWidth + 2 - leverWidth;

leverZDisp = 2.6;
leverDepth = 4.3;

limitSwitch();

module limitSwitch() {
    color([0.3, 0.3, 0.3]) difference() {
        //main body
        roundedSquare(switchCornerR, [switchLength, switchWidth, switchDepth]);
        
        //mount holes
        for(loc = mountHoleLocs) 
            translate([loc[0], loc[1], -1]) cylinder(switchDepth+2, r=mountHoleR, $fn=fid);
    }
    
    //tab1
    color([0.7, 0.7, 0.7]) 
        translate([-tab12Length, tab1YDisp, tabZDisp])
        cube([tab12Length, tabWidth, tabDepth]);
    
    //tab2
    color([0.7, 0.7, 0.7]) 
        translate([-tab12Length, tab2YDisp, tabZDisp])
        cube([tab12Length, tabWidth, tabDepth]);
    
    //tab3
    color([0.7, 0.7, 0.7])
        translate([tab3XDisp-tab3Length, tab3YDisp, tabZDisp])
        cube([tab3Length, tabWidth, tabDepth]);
    //the connecting segment for tab3
    color([0.7, 0.7, 0.7])
        translate([tab3XDisp-tabWidth, tab3YDisp, tabZDisp])
        cube([tabWidth, -tab3YDisp, tabDepth]);
    
    //lever
    color([0.7, 0.7, 0.7])
        translate([leverXDisp, leverYDisp, leverZDisp])
        cube([leverLength, leverWidth, leverDepth]);
    //connecting segment for the lever
    color([0.7, 0.7, 0.7])
        translate([leverXDisp, switchWidth, leverZDisp])
        cube([leverWidth, leverYDisp-switchWidth, leverDepth]);
}

function limitSwitchMountHoles() = mountHoleLocs;

module roundedSquare(r, vec) {
    hull() {
        translate([r,r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([vec[0]-r,r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([r,vec[1]-r,0]) cylinder(vec[2], r, r, $fn=fid);
        translate([vec[0]-r,vec[1]-r,0]) cylinder(vec[2], r, r, $fn=fid);
    }
}